﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Lab1
{
    class Functions
    {
        public List<Color> ColorSubSys = new List<Color>();
        public void AlgTopologDecompos(DataGridView DGV, DataGridView DGV6)
        {
            List<List<int>> G = new List<List<int>>();
            
            List<List<int>> R = new List<List<int>>();
            for (int i = 0; i < DGV.RowCount; i++)
            {
                List<int> one_wayR = new List<int>();
                one_wayR.Add(i);
                int counter = 0, now_el;
                bool Access;
                while (counter<one_wayR.Count)
                {
                    now_el = one_wayR[counter];
                    for (int j = 0; j < DGV.ColumnCount; j++)
                    {
                        Access = true;
                        if (Convert.ToInt32(DGV[j, now_el].Value) == 1)
                        {
                            for (int s = 0; s < one_wayR.Count; s++)
                            {
                                if (j == one_wayR[s])
                                {
                                    Access = false;
                                    break;
                                }
                            }
                            if (Access)
                            {
                                one_wayR.Add(j);
                            }
                        }
                    }
                    counter++;
                }
                R.Add(one_wayR);
            }

            List<List<int>> Q = new List<List<int>>();
            for (int i = 0; i < DGV.ColumnCount; i++)
            {
                List<int> one_wayQ = new List<int>();
                one_wayQ.Add(i);
                int counter = 0, now_el;
                bool Access;
                while (counter < one_wayQ.Count)
                {
                    now_el = one_wayQ[counter];
                    for (int j = 0; j < DGV.RowCount; j++)
                    {
                        Access = true;
                        if (Convert.ToInt32(DGV[now_el, j].Value) == 1)
                        {
                            for (int s = 0; s < one_wayQ.Count; s++)
                            {
                                if (j == one_wayQ[s])
                                {
                                    Access = false;
                                    break;
                                }
                            }
                            if (Access)
                            {
                                one_wayQ.Add(j);
                            }
                        }
                    }
                    counter++;
                }
                Q.Add(one_wayQ);
            }


            for(int i=0; i<DGV.ColumnCount; i++)
            {
                List<int> one_wayG = new List<int>();
                for (int k = 0; k < R[i].Count; k++)
                {
                    for(int s = 0; s < Q[i].Count; s++)
                    {
                        if(R[i][k]==Q[i][s])
                        {
                            one_wayG.Add(R[i][k]);
                        }
                    }
                }
                G.Add(one_wayG);
            }

            int sovpad = 0;
            for(int i=0; i<G.Count-1; i++)
            {
                for(int j=i+1; j<G.Count; j++)
                {
                    if (G[i].Count == G[j].Count)
                    {
                        for (int s = 0; s < G[i].Count; s++)
                        {
                            for (int d = 0; d < G[j].Count; d++)
                            {
                                if (G[i][s] == G[j][d])
                                {
                                    sovpad++;
                                }
                            }
                        }
                        if(sovpad==G[i].Count)
                        {
                            sovpad = 0;
                            G.RemoveAt(j);
                            j--;
                        }
                    }
                }
            }

            int max_el = 0;
            for(int i=0; i<G.Count; i++)
            {
                if(G[i].Count>max_el)
                {
                    max_el = G[i].Count;
                }
            }

            DGV6.ColumnCount = max_el+1;
            DGV6.RowCount = G.Count;
            DGV6.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            DGV6.Columns[0].HeaderText = "№ подсистемы";
            DGV6.Columns[0].Width = 90;
            Random rand = new Random();
            for (int i = 0; i < DGV6.RowCount; i++)
            {
                ColorSubSys.Add(Color.FromArgb(rand.Next() % 235, rand.Next() % 245, rand.Next() % 255));
            }
            
            for (int i=0; i<DGV6.RowCount; i++)
            {
                DGV6[0, i].Value = i + 1;
                DGV6[0, i].Style.BackColor = ColorSubSys[i];
                DGV6.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                for (int s=0; s<G[i].Count; s++)
                {
                    DGV6[s + 1, i].Value = G[i][s];
                    DGV6.Columns[s+1].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
        }
    }
}