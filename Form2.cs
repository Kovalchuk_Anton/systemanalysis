﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Lab1
{
    public partial class Form2 : Form
    {
        //Входные данные
        Functions atd;
        List<Rectangle> ListRect, ListRect2;
        List<Color> ListColor;
        List<Point> ListPoints, deltaS = new List<Point>();
        List<List<int>> ListLevel;
        List<int> KratchPath;
        DataGridView Graph; //только для отрисовки
        List<Rectangle> GraphListRect; //только для отрисовки
        List<Color> GraphListColor; //только для отрисовки
        bool Successful = false, Successful1 = false, Access_PB = false, Access_PB4 = false, DrawAccess = false, Svyaznost = false;
        int deltaX, deltaY, objID;

        public Form2() //Начальное состояние формы
        {
            InitializeComponent();
            panel3.Hide();
            panel4.Hide();
            button2.Hide();

            dataGridView4.Visible = false;
            label4.Visible = false;
            dataGridView5.Visible = false;
            label1.Visible = false;
            label3.Visible = false;

            label2.Visible = false;

            label11.Visible = false;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e) //Отрисовка графа
        {
            if (Access_PB)
            {
                //Рисуем сначала связи
                int counter = 0, x_left = 0, y_left = 0, x_right, y_right;
                Pen pen;
                for (int j = 0; j < Graph.ColumnCount; j++)
                {
                    for (int i = 0; i < Graph.RowCount; i++)
                    {
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j && Convert.ToInt32(Graph[i, j].Value) != 1)
                        {
                            x_right = GraphListRect[i].X + 25;
                            y_right = GraphListRect[i].Y + 25;
                            x_left = GraphListRect[j].X + 25;
                            y_left = GraphListRect[j].Y + 25;
                            pen = new Pen(GraphListColor[counter], 6);
                            pen.EndCap = LineCap.ArrowAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            Point[] points = new Point[2];
                            double Y = 0, X = 0;
                            if ((y_right - y_left) != 0 && (x_right - x_left) != 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + x_left;
                                }
                                else
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + x_left;
                                }

                            }
                            if ((y_right - y_left) == 0 && (x_right - x_left) != 0)
                            {
                                Y = y_left;
                                if (x_left < x_right)
                                {
                                    X = x_left + 25;
                                }
                                else
                                {
                                    X = x_left - 25;
                                }
                            }
                            if ((y_right - y_left) != 0 && (x_right - x_left) == 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = y_left + 25;
                                }
                                else
                                {
                                    Y = y_left - 25;
                                }
                                X = x_left;
                            }
                            e.Graphics.DrawLine(pen, x_right, y_right, (int)X, (int)Y);
                            counter++;
                        }
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j && Convert.ToInt32(Graph[i, j].Value) == 1)
                        {
                            x_right = GraphListRect[i].X + 25;
                            y_right = GraphListRect[i].Y + 25;
                            x_left = GraphListRect[j].X + 25;
                            y_left = GraphListRect[j].Y + 25;
                            pen = new Pen(GraphListColor[counter], 6);
                            pen.EndCap = LineCap.RoundAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            e.Graphics.DrawLine(pen, x_right, y_right, x_left, y_left);
                            counter++;
                        }
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i == j)
                        {
                            x_left = GraphListRect[i].X + 25;
                            y_left = GraphListRect[i].Y + 25;
                            pen = new Pen(GraphListColor[counter], 6);
                            pen.EndCap = LineCap.ArrowAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            Point start = new Point(x_left, y_left);
                            Point control1 = new Point(x_left - 60, y_left - 60);
                            Point control2 = new Point(x_left + 50, y_left - 50);
                            Point end = new Point(x_left + 15, y_left - 20);
                            e.Graphics.DrawBezier(pen, start, control1, control2, end);
                            counter++;
                        }
                    }
                }

                //Поверх связей рисуем узлы и номера узлов
                SolidBrush SBrush = new SolidBrush(Color.Green);
                int X_smesh = 11;
                for (int i = 0; i < GraphListRect.Count; i++)
                {
                    e.Graphics.FillEllipse(SBrush, GraphListRect[i]);
                    e.Graphics.DrawEllipse(new Pen(Color.Black, 3), GraphListRect[i]);
                    if (i < 10) { X_smesh = 11; }
                    if (i >= 10 && i < 100) { X_smesh = 3; }
                    e.Graphics.DrawString(Convert.ToString(i), new Font("Microsoft Sans Serif", 24), new SolidBrush(Color.White), GraphListRect[i].X + X_smesh, GraphListRect[i].Y + 7);
                }
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (GraphListRect != null)
                {
                    for (int i = 0; i < GraphListRect.Count; i++)
                    {
                        if (e.X > GraphListRect[i].X && e.X < GraphListRect[i].X + GraphListRect[i].Width && e.Y > GraphListRect[i].Y && e.Y < GraphListRect[i].Y + GraphListRect[i].Height)
                        {
                            objID = i;
                            deltaX = e.X - GraphListRect[i].X;
                            deltaY = e.Y - GraphListRect[i].Y;
                            pictureBox1.Cursor = Cursors.Hand;
                            Successful = true;
                        }
                    }
                    if (Successful == false)
                    {
                        deltaS = new List<Point>();
                        for (int i = 0; i < GraphListRect.Count; i++)
                        {
                            deltaS.Add(new Point(e.X - GraphListRect[i].X, e.Y - GraphListRect[i].Y));
                        }
                        pictureBox1.Cursor = Cursors.SizeAll;
                        Successful1 = true;
                    }
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rect;
            if (Successful)
            {
                rect = GraphListRect[objID];
                rect.X = e.X - deltaX;
                rect.Y = e.Y - deltaY;
                GraphListRect[objID] = rect;
                pictureBox1.Invalidate();
            }
            if (Successful1)
            {
                for (int i = 0; i < GraphListRect.Count; i++)
                {
                    rect = GraphListRect[i];
                    rect.X = e.X - deltaS[i].X;
                    rect.Y = e.Y - deltaS[i].Y;
                    GraphListRect[i] = rect;
                }
                pictureBox1.Invalidate();
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            //Обозначаем, что мы больше не перетаскиваем узел
            Successful = false;

            Successful1 = false;

            pictureBox1.Cursor = Cursors.Arrow;
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e) //Отрисовка иерархических уровней
        {
            if (DrawAccess)
            {
                //Рисуем линии уровней и номера уровней
                Pen pen = new Pen(Color.Black, 4);
                e.Graphics.DrawLine(pen, 20, 40, ListLevel.Count * 100 - 10, 40);
                for (int i = 0; i < ListLevel.Count; i++)
                {
                    pen.DashStyle = DashStyle.Dash;
                    e.Graphics.DrawLine(pen, 45 + i * 100, 43, 45 + i * 100, 30 + ListLevel[i].Count * 100);
                    e.Graphics.DrawString(Convert.ToString(i), new Font("Microsoft Sans Serif", 24), new SolidBrush(Color.Black), 27 + i * 100, 7);
                }

                //Рисуем сначала связи
                int counter = 0, x_left = 0, y_left = 0, x_right = 0, y_right = 0;
                for (int j = 0; j < Graph.ColumnCount; j++)
                {
                    for (int i = 0; i < Graph.RowCount; i++)
                    {
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j)
                        {
                            int elements = 0;
                            for (int s = 0; s < ListLevel.Count; s++)
                            {
                                for (int k = 0; k < ListLevel[s].Count; k++)
                                {
                                    if (i == ListLevel[s][k])
                                    {
                                        elements = 0;
                                        int l = 0;
                                        while (l != s)
                                        {
                                            elements += ListLevel[l].Count;
                                            l++;
                                        }
                                        x_right = ListRect2[elements + k].X + 25;
                                        y_right = ListRect2[elements + k].Y + 25;
                                    }
                                    if (j == ListLevel[s][k])
                                    {
                                        elements = 0;
                                        int l = 0;
                                        while (l != s)
                                        {
                                            elements += ListLevel[l].Count;
                                            l++;
                                        }
                                        x_left = ListRect2[elements + k].X + 25;
                                        y_left = ListRect2[elements + k].Y + 25;
                                    }
                                }
                            }
                            pen = new Pen(GraphListColor[counter], 6);
                            pen.DashStyle = DashStyle.Solid;
                            pen.EndCap = LineCap.ArrowAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            Point[] points = new Point[2];
                            double Y = 0, X = 0;
                            if ((y_right - y_left) != 0 && (x_right - x_left) != 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + x_left;
                                }
                                else
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + x_left;
                                }

                            }
                            if ((y_right - y_left) == 0 && (x_right - x_left) != 0)
                            {
                                Y = y_left;
                                if (x_left < x_right)
                                {
                                    X = x_left + 25;
                                }
                                else
                                {
                                    X = x_left - 25;
                                }
                            }
                            if ((y_right - y_left) != 0 && (x_right - x_left) == 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = y_left + 25;
                                }
                                else
                                {
                                    Y = y_left - 25;
                                }
                                X = x_left;
                            }
                            if ((int)X - x_right > 75 && (int)Y == y_right)
                            {
                                Point[] CurvePoints = new Point[3];
                                CurvePoints[0] = new Point(x_right, y_right);
                                CurvePoints[1] = new Point(x_right + ((int)X - x_right) / 2, y_right + 40);
                                CurvePoints[2] = new Point((int)X, (int)Y);
                                e.Graphics.DrawCurve(pen, CurvePoints);
                            }
                            else
                            {
                                e.Graphics.DrawLine(pen, x_right, y_right, (int)X, (int)Y);
                            }
                            counter++;
                        }
                    }
                }

                //Поверх связей рисуем узлы и номера узлов
                SolidBrush SBrush = new SolidBrush(Color.DarkGreen);
                counter = 0;
                for (int i = 0; i < ListLevel.Count; i++)
                {
                    for (int j = 0; j < ListLevel[i].Count; j++)
                    {
                        e.Graphics.FillEllipse(SBrush, ListRect2[counter]);
                        e.Graphics.DrawString(counter + "(" + Convert.ToString(ListLevel[i][j]) + ")", new Font("Microsoft Sans Serif", 14), new SolidBrush(Color.White), ListRect2[counter].X + 3, ListRect2[counter].Y + 11);
                        counter++;
                    }
                }
            }
        }

        private void pictureBox3_Paint(object sender, PaintEventArgs e) //Отрисовка подсистем
        {
            if (Access_PB)
            {
                //Рисуем сначала связи
                int counter = 0, x_left = 0, y_left = 0, x_right, y_right;
                Pen pen;
                for (int j = 0; j < Graph.ColumnCount; j++)
                {
                    for (int i = 0; i < Graph.RowCount; i++)
                    {
                        //Сначала рисуем связи
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j && Convert.ToInt32(Graph[i, j].Value) != 1)
                        {
                            x_right = GraphListRect[i].X + 25;
                            y_right = GraphListRect[i].Y + 25;
                            x_left = GraphListRect[j].X + 25;
                            y_left = GraphListRect[j].Y + 25;
                            pen = new Pen(Color.Black, 6);
                            pen.EndCap = LineCap.ArrowAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            Point[] points = new Point[2];
                            double Y = 0, X = 0;
                            if ((y_right - y_left) != 0 && (x_right - x_left) != 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + x_left;
                                }
                                else
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + x_left;
                                }

                            }
                            if ((y_right - y_left) == 0 && (x_right - x_left) != 0)
                            {
                                Y = y_left;
                                if (x_left < x_right)
                                {
                                    X = x_left + 25;
                                }
                                else
                                {
                                    X = x_left - 25;
                                }
                            }
                            if ((y_right - y_left) != 0 && (x_right - x_left) == 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = y_left + 25;
                                }
                                else
                                {
                                    Y = y_left - 25;
                                }
                                X = x_left;
                            }
                            e.Graphics.DrawLine(pen, x_right, y_right, (int)X, (int)Y);
                            counter++;
                        }
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j && Convert.ToInt32(Graph[i, j].Value) == 1)
                        {
                            x_right = GraphListRect[i].X + 25;
                            y_right = GraphListRect[i].Y + 25;
                            x_left = GraphListRect[j].X + 25;
                            y_left = GraphListRect[j].Y + 25;
                            pen = new Pen(Color.Black, 6);
                            pen.EndCap = LineCap.RoundAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            e.Graphics.DrawLine(pen, x_right, y_right, x_left, y_left);
                            counter++;
                        }
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i == j)
                        {
                            x_left = GraphListRect[i].X + 25;
                            y_left = GraphListRect[i].Y + 25;
                            pen = new Pen(Color.Black, 6);
                            pen.EndCap = LineCap.ArrowAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            Point start = new Point(x_left, y_left);
                            Point control1 = new Point(x_left - 60, y_left - 60);
                            Point control2 = new Point(x_left + 50, y_left - 50);
                            Point end = new Point(x_left + 15, y_left - 20);
                            e.Graphics.DrawBezier(pen, start, control1, control2, end);
                            counter++;
                        }
                    }
                }

                //Поверх связей рисуем узлы и номера узлов
                SolidBrush SBrush;
                int X_smesh = 11;
                for (int i = 0; i < GraphListRect.Count; i++)
                {
                    SBrush = null;
                    for (int k=0; k<dataGridView6.RowCount; k++)
                    {
                        for (int s = 1; s < dataGridView6.ColumnCount; s++)
                        {
                            if (dataGridView6[s, k].Value != null)
                            {
                                if(Convert.ToInt32(dataGridView6[s, k].Value)==i)
                                {
                                    SBrush = new SolidBrush(atd.ColorSubSys[k]);
                                    break;
                                }
                            }
                        }
                        if(SBrush != null)
                        {
                            break;
                        }
                    }
                    e.Graphics.FillEllipse(SBrush, GraphListRect[i]);
                    e.Graphics.DrawEllipse(new Pen(Color.Black, 3), GraphListRect[i]);
                    if (i < 10) { X_smesh = 11; }
                    if (i >= 10 && i < 100) { X_smesh = 3; }
                    e.Graphics.DrawString(Convert.ToString(i), new Font("Microsoft Sans Serif", 24), new SolidBrush(Color.White), GraphListRect[i].X + X_smesh, GraphListRect[i].Y + 7);
                }
            }
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (GraphListRect != null)
                {
                    for (int i = 0; i < GraphListRect.Count; i++)
                    {
                        if (e.X > GraphListRect[i].X && e.X < GraphListRect[i].X + GraphListRect[i].Width && e.Y > GraphListRect[i].Y && e.Y < GraphListRect[i].Y + GraphListRect[i].Height)
                        {
                            objID = i;
                            deltaX = e.X - GraphListRect[i].X;
                            deltaY = e.Y - GraphListRect[i].Y;
                            pictureBox3.Cursor = Cursors.Hand;
                            Successful = true;
                        }
                    }
                    if (Successful == false)
                    {
                        deltaS = new List<Point>();
                        for (int i = 0; i < GraphListRect.Count; i++)
                        {
                            deltaS.Add(new Point(e.X - GraphListRect[i].X, e.Y - GraphListRect[i].Y));
                        }
                        pictureBox3.Cursor = Cursors.SizeAll;
                        Successful1 = true;
                    }
                }
            }
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rect;
            if (Successful)
            {
                rect = GraphListRect[objID];
                rect.X = e.X - deltaX;
                rect.Y = e.Y - deltaY;
                GraphListRect[objID] = rect;
                pictureBox3.Invalidate();
            }
            if (Successful1)
            {
                for (int i = 0; i < GraphListRect.Count; i++)
                {
                    rect = GraphListRect[i];
                    rect.X = e.X - deltaS[i].X;
                    rect.Y = e.Y - deltaS[i].Y;
                    GraphListRect[i] = rect;
                }
                pictureBox3.Invalidate();
            }
        }

        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            //Обозначаем, что мы больше не перетаскиваем узел
            Successful = false;

            Successful1 = false;

            pictureBox3.Cursor = Cursors.Arrow;
        }
        
        private void pictureBox4_Paint(object sender, PaintEventArgs e) //Отрисовка кратчайшего пути
        {
            if (Access_PB4)
            {
                //Рисуем сначала связи
                int counter = 0, x_left = 0, y_left = 0, x_right, y_right;
                Pen pen;
                for (int j = 0; j < Graph.ColumnCount; j++)
                {
                    for (int i = 0; i < Graph.RowCount; i++)
                    {
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j && Convert.ToInt32(Graph[i, j].Value) != 1)
                        {
                            x_right = GraphListRect[i].X + 25;
                            y_right = GraphListRect[i].Y + 25;
                            x_left = GraphListRect[j].X + 25;
                            y_left = GraphListRect[j].Y + 25;
                            pen = new Pen(Color.Black, 6);
                            for (int s=0; s<KratchPath.Count-1; s++)
                            {
                                if(i == KratchPath[s] && j == KratchPath[s +1])
                                {
                                    pen = new Pen(Color.PaleTurquoise, 6);
                                }
                            }
                            
                            pen.EndCap = LineCap.ArrowAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            Point[] points = new Point[2];
                            double Y = 0, X = 0;
                            if ((y_right - y_left) != 0 && (x_right - x_left) != 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) + x_left;
                                }
                                else
                                {
                                    Y = Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + y_left;
                                    X = ((double)(x_right - x_left) / (y_right - y_left)) * Math.Sqrt((25 * 25) / (((double)(x_right - x_left) / (y_right - y_left)) * ((double)(x_right - x_left) / (y_right - y_left)) + 1)) * (-1) + x_left;
                                }

                            }
                            if ((y_right - y_left) == 0 && (x_right - x_left) != 0)
                            {
                                Y = y_left;
                                if (x_left < x_right)
                                {
                                    X = x_left + 25;
                                }
                                else
                                {
                                    X = x_left - 25;
                                }
                            }
                            if ((y_right - y_left) != 0 && (x_right - x_left) == 0)
                            {
                                if (y_left < y_right)
                                {
                                    Y = y_left + 25;
                                }
                                else
                                {
                                    Y = y_left - 25;
                                }
                                X = x_left;
                            }
                            e.Graphics.DrawLine(pen, x_right, y_right, (int)X, (int)Y);
                            counter++;
                        }
                        if (Convert.ToInt32(Graph[j, i].Value) == 1 && i != j && Convert.ToInt32(Graph[i, j].Value) == 1)
                        {
                            x_right = GraphListRect[i].X + 25;
                            y_right = GraphListRect[i].Y + 25;
                            x_left = GraphListRect[j].X + 25;
                            y_left = GraphListRect[j].Y + 25;
                            pen = new Pen(Color.Black, 6);
                            for (int s = 0; s < KratchPath.Count - 1; s++)
                            {
                                if (i == KratchPath[s] && j == KratchPath[s + 1])
                                {
                                    pen = new Pen(Color.PaleTurquoise, 6);
                                }
                            }

                            pen.EndCap = LineCap.RoundAnchor;
                            pen.StartCap = LineCap.RoundAnchor;
                            e.Graphics.DrawLine(pen, x_right, y_right, x_left, y_left);
                            counter++;
                        }
                    }
                }

                //Поверх связей рисуем узлы и номера узлов
                int X_smesh = 11;
                for (int i = 0; i < GraphListRect.Count; i++)
                {
                    SolidBrush SBrush = new SolidBrush(Color.Black);
                    for (int s = 0; s < KratchPath.Count; s++)
                    {
                        if (i == KratchPath[s])
                        {
                            SBrush = new SolidBrush(Color.PaleTurquoise);
                        }
                    }
                    e.Graphics.FillEllipse(SBrush, GraphListRect[i]);
                    e.Graphics.DrawEllipse(new Pen(Color.Black, 3), GraphListRect[i]);
                    if (i < 10) { X_smesh = 11; }
                    if (i >= 10 && i < 100) { X_smesh = 3; }
                    e.Graphics.DrawString(Convert.ToString(i), new Font("Microsoft Sans Serif", 24), new SolidBrush(Color.White), GraphListRect[i].X + X_smesh, GraphListRect[i].Y + 7);
                }
            }
        }

        private void pictureBox4_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (GraphListRect != null)
                {
                    for (int i = 0; i < GraphListRect.Count; i++)
                    {
                        if (e.X > GraphListRect[i].X && e.X < GraphListRect[i].X + GraphListRect[i].Width && e.Y > GraphListRect[i].Y && e.Y < GraphListRect[i].Y + GraphListRect[i].Height)
                        {
                            objID = i;
                            deltaX = e.X - GraphListRect[i].X;
                            deltaY = e.Y - GraphListRect[i].Y;
                            pictureBox4.Cursor = Cursors.Hand;
                            Successful = true;
                        }
                    }
                    if (Successful == false)
                    {
                        deltaS = new List<Point>();
                        for (int i = 0; i < GraphListRect.Count; i++)
                        {
                            deltaS.Add(new Point(e.X - GraphListRect[i].X, e.Y - GraphListRect[i].Y));
                        }
                        pictureBox4.Cursor = Cursors.SizeAll;
                        Successful1 = true;
                    }
                }
            }
        }

        private void pictureBox4_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rect;
            if (Successful)
            {
                rect = GraphListRect[objID];
                rect.X = e.X - deltaX;
                rect.Y = e.Y - deltaY;
                GraphListRect[objID] = rect;
                pictureBox4.Invalidate();
            }
            if (Successful1)
            {
                for (int i = 0; i < GraphListRect.Count; i++)
                {
                    rect = GraphListRect[i];
                    rect.X = e.X - deltaS[i].X;
                    rect.Y = e.Y - deltaS[i].Y;
                    GraphListRect[i] = rect;
                }
                pictureBox4.Invalidate();
            }
        }

        private void pictureBox4_MouseUp(object sender, MouseEventArgs e)
        {
            //Обозначаем, что мы больше не перетаскиваем узел
            Successful = false;

            Successful1 = false;

            pictureBox4.Cursor = Cursors.Arrow;
        }
        
        private void button5_Click(object sender, EventArgs e) //Задание матрицы инцидентности
        {
            int v_count, e_count;
            try
            {
                v_count = Convert.ToInt32(textBox3.Text);
                e_count = Convert.ToInt32(textBox4.Text);
                if (v_count > 0 && v_count < 101 && (e_count > 0 && e_count <= (v_count + v_count * (v_count - 1))))
                {
                    dataGridView2.ColumnCount = e_count;
                    dataGridView2.RowCount = v_count;
                    dataGridView2.ColumnHeadersVisible = true;
                    dataGridView2.RowHeadersVisible = true;
                    dataGridView2.RowHeadersWidth = 60;
                    for (int i = 0; i < e_count; i++)
                    {
                        dataGridView2.Columns[i].HeaderText = "e[" + i + "]";
                        dataGridView2.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                    for (int i = 0; i < v_count; i++)
                    {
                        dataGridView2.Rows[i].HeaderCell.Value = "v[" + i + "]";
                    }
                    panel3.Show();
                }
                else
                {
                    MessageBox.Show("Кол-во узлов N - это натуральное число от 1 до 100 включительно,\nа кол-во связей E - это натуральное число, удволетворяющее следующему условию:\n0<E<(N+N*(N-1)).", "Ошибка");
                }
            }
            catch
            {
                MessageBox.Show("Введите в поля натуральные числа не равные 0.", "Ошибка");
            }
        }

        private void button7_Click(object sender, EventArgs e) //Проверка, заполнение матриц, положение узлов,генерация цветов, разрешение рисовать, и запуск функций.
        {
            int ed = 0, minus_ed = 0;
            bool Successful = true;
            try
            {
                for (int j = 0; j < dataGridView2.ColumnCount; j++) //Первая стадия: проверка значений
                {
                    for (int i = 0; i < dataGridView2.RowCount; i++)
                    {
                        if (dataGridView2[j, i].Value == null)
                        {
                            dataGridView2[j, i].Value = 0;
                        }
                        if (Convert.ToInt32(dataGridView2[j, i].Value) != 0 && Convert.ToInt32(dataGridView2[j, i].Value) != 1 && Convert.ToInt32(dataGridView2[j, i].Value) != -1)
                        {
                            Successful = false;
                            break;
                        }
                        if (Convert.ToInt32(dataGridView2[j, i].Value) == 1) ed++;
                        if (Convert.ToInt32(dataGridView2[j, i].Value) == -1) minus_ed++;
                    }
                    if ((ed == 1 && minus_ed == 1) || (ed == 1 && minus_ed == 0))
                    {
                        ed = 0;
                        minus_ed = 0;
                    }
                    else
                    {
                        Successful = false;
                    }
                    if (Successful == false)
                    {
                        MessageBox.Show("В каждом столбце должна быть одна единица или \"1\" и \"-1\" в двух разных ячейках столбца.\nДругие ячейки можно не заполнять или заполнить нулями.", "Ошибка");
                        break;
                    }
                }
                int razlich = 0;
                if (Successful == true)
                {
                    for (int i = dataGridView2.ColumnCount - 1; i > 0; i--) //Вторая стадия: проверка дублирующихся связей
                    {
                        for (int j = i - 1; j >= 0; j--)
                        {
                            for (int v = 0; v < dataGridView2.RowCount; v++)
                            {
                                if (Convert.ToInt32(dataGridView2[i, v].Value) != Convert.ToInt32(dataGridView2[j, v].Value))
                                {
                                    razlich++;
                                }
                            }
                            if (razlich == 0)
                            {
                                dataGridView2.Columns.RemoveAt(j);
                                i--;
                                Successful = false;
                            }
                            razlich = 0;
                        }
                    }
                    if (Successful == false)
                    {
                        MessageBox.Show("Были найдены и удалены повторяющиеся столбцы. Проверьте матрицу инцидентности.\nЕсли вы не правильно задали граф, вернитесь назад и перезадайте его.", "Оповещение");
                        Successful = true;
                        textBox4.Text = Convert.ToString(dataGridView2.ColumnCount);
                    }
                }
            }
            catch
            {
                Successful = false;
                MessageBox.Show("В каждом столбце должна быть одна единица или \"1\" и \"-1\" в двух разных ячейках столбца.\nДругие ячейки можно не заполнять или заполнить нулями.", "Ошибка");
            }
            //Если таблица заполнена верно, то открывается окно выходных данных
            if (Successful == true)
            {
                //Формируем лист рандомных цветов для связей
                Random rand = new Random();
                ListColor = new List<Color>();
                for (int i = 0; i < dataGridView2.ColumnCount; i++)
                {
                    ListColor.Add(Color.FromArgb(rand.Next() % 235, rand.Next() % 245, rand.Next() % 255));
                }

                //Матрица смежности
                dataGridView3.RowCount = 0;
                dataGridView3.ColumnCount = 0;
                dataGridView3.ColumnCount = dataGridView2.RowCount;
                dataGridView3.RowCount = dataGridView2.RowCount;
                dataGridView3.ColumnHeadersVisible = true;
                dataGridView3.RowHeadersVisible = true;
                dataGridView3.RowHeadersWidth = 60;
                for (int i = 0; i < dataGridView3.RowCount; i++)
                {
                    dataGridView3.Columns[i].HeaderText = Convert.ToString(dataGridView2.Rows[i].HeaderCell.Value);
                    dataGridView3.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dataGridView3.Rows[i].HeaderCell.Value = dataGridView2.Rows[i].HeaderCell.Value;
                }
                int counter = 0, v_numb = 0;
                for (int i = 0; i < dataGridView2.ColumnCount; i++)
                {
                    for (int j = 0; j < dataGridView2.RowCount; j++)
                    {
                        if ((Convert.ToInt32(dataGridView2[i, j].Value) == 1 || Convert.ToInt32(dataGridView2[i, j].Value) == -1) && counter == 0)
                        {
                            v_numb = j;
                            counter++;
                            continue;
                        }
                        if (Convert.ToInt32(dataGridView2[i, j].Value) == Convert.ToInt32(dataGridView2[i, v_numb].Value) * (-1) && counter == 1)
                        {
                            if (Convert.ToInt32(dataGridView2[i, v_numb].Value) == 1) dataGridView3[j, v_numb].Value = 1;
                            if (Convert.ToInt32(dataGridView2[i, v_numb].Value) == -1) dataGridView3[v_numb, j].Value = 1;
                            counter = 0;
                            break;
                        }
                    }
                    if (counter == 1)
                    {
                        dataGridView3[v_numb, v_numb].Value = 1;
                        counter = 0;
                    }
                }
                for (int i = 0; i < dataGridView3.ColumnCount; i++)
                {
                    for (int j = 0; j < dataGridView3.RowCount; j++)
                    {
                        if (dataGridView3[i, j].Value == null)
                        {
                            dataGridView3[i, j].Value = 0;
                        }
                    }
                }
                textBox1.Text = Convert.ToString(dataGridView3.RowCount);
                panel4.Show();

                //Высчитываем размеры матрицы, в которой будем располагать узлы
                int N = 0;
                for (int i = 2; i <= 10; i++)
                {
                    if (dataGridView2.RowCount <= i * i)
                    {
                        N = i;
                        break;
                    }
                }

                //Создаем прямоугольники, в которых будем располагать узлы и помещаем в List
                ListRect = new List<Rectangle>();
                counter = 0;
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        ListRect.Add(new Rectangle(10 + j * 100, 25 + i * 100, 50, 50));
                        counter++;
                        if (counter == dataGridView2.RowCount)
                        {
                            break;
                        }
                    }
                    if (counter == dataGridView2.RowCount)
                    {
                        counter = 0;
                        break;
                    }
                }

                if (ListPoints != null)
                {
                    Rectangle rect;
                    for (int i = 0; i < ListRect.Count; i++)
                    {
                        rect = ListRect[i];
                        rect.X = ListPoints[i].X;
                        rect.Y = ListPoints[i].Y;
                        ListRect[i] = rect;
                    }
                    ListPoints = null;
                }
                Access_PB = false;
                Access_PB4 = false;
                DrawAccess = false;
                Thread.Sleep(300);

                Graph = new DataGridView();
                Graph.ColumnCount = dataGridView3.ColumnCount;
                Graph.RowCount = dataGridView3.RowCount;
                for(int i=0; i<Graph.RowCount; i++)
                {
                    for (int j = 0; j < Graph.ColumnCount; j++)
                    {
                        Graph[j, i].Value = Convert.ToInt32(dataGridView3[j,i].Value);
                    }
                }

                GraphListRect = new List<Rectangle>();
                for(int i = 0; i < ListRect.Count; i++)
                {
                    GraphListRect.Add(ListRect[i]);
                }

                GraphListColor = new List<Color>();
                for (int i = 0; i < ListColor.Count; i++)
                {
                    GraphListColor.Add(ListColor[i]);
                }

                button2.Show();

                atd = new Functions();
                atd.AlgTopologDecompos(dataGridView3, dataGridView6);

                Access_PB = true;


                StructTopologCharact();
                IsolationLevel();
                Shortest_Paths();

                pictureBox1.Invalidate();
                pictureBox2.Invalidate();
            }
        }
        
        private void button8_Click(object sender, EventArgs e) //Удаление узла (Матр. инцидентности)
        {
            try
            {
                int el_v = Convert.ToInt32(textBox5.Text);
                if (el_v>=0 && el_v < dataGridView2.RowCount)
                {
                    if (dataGridView2.RowCount != 1)
                    {
                        dataGridView2.Rows.RemoveAt(el_v);
                        for (int i = 0; i < dataGridView2.RowCount; i++)
                        {
                            dataGridView2.Rows[i].HeaderCell.Value = "v[" + i + "]";
                        }
                        textBox3.Text = Convert.ToString(dataGridView2.RowCount);
                    }
                    else
                    {
                        MessageBox.Show("Нельзя удалить последнюю связь или последний узел в графе.", "Ошибка ввода");
                    }
                }
                else
                {
                    MessageBox.Show("Нет узла с таким номером. Найдите строку в таблице,\nкоторую хотите удалить, посмотрите ее номер в колонке слева\nи введите в поле для удаления.", "Ошибка ввода");
                }
            }
            catch
            {
                MessageBox.Show("Для удаления строки введите ее номер (натуральное число) в поле.", "Ошибка ввода");
            }
            textBox5.Text = "";
        }
        
        private void button9_Click(object sender, EventArgs e) //Удаление связи (Матр. инцидентности)
        {
            try
            {
                int el_e = Convert.ToInt32(textBox6.Text);
                if (el_e >= 0 && el_e < dataGridView2.ColumnCount)
                {
                    if (dataGridView2.ColumnCount != 1)
                    {
                        dataGridView2.Columns.RemoveAt(el_e);
                        for (int i = 0; i < dataGridView2.ColumnCount; i++)
                        {
                            dataGridView2.Columns[i].HeaderText = "e[" + i + "]";
                        }
                        textBox4.Text = Convert.ToString(dataGridView2.ColumnCount);
                    }
                    else
                    {
                        MessageBox.Show("Нельзя удалить последнюю связь или последний узел в графе.", "Ошибка ввода");
                    }
                }
                else
                {
                    MessageBox.Show("Нет связи с таким номером. Найдите столбец в таблице,\nкоторый хотите удалить, посмотрите его номер в строке сверху\nи введите в поле для удаления.", "Ошибка ввода");
                }
            }
            catch
            {
                MessageBox.Show("Для удаления столбца введите его номер (натуральное число) в поле.", "Ошибка ввода");
            }
            textBox6.Text = "";
        }

        private void button3_Click(object sender, EventArgs e) //Задание матрицы смежности
        {
            int v_count;
            try
            {
                v_count = Convert.ToInt32(textBox1.Text);
                if (v_count > 0 && v_count < 101)
                {
                    dataGridView3.ColumnCount = v_count;
                    dataGridView3.RowCount = v_count;
                    dataGridView3.ColumnHeadersVisible = true;
                    dataGridView3.RowHeadersVisible = true;
                    dataGridView3.RowHeadersWidth = 60;
                    for (int i = 0; i < v_count; i++)
                    {
                        dataGridView3.Columns[i].HeaderText = "v[" + i + "]";
                        dataGridView3.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                        dataGridView3.Rows[i].HeaderCell.Value = "v[" + i + "]";
                    }
                    panel4.Show();
                }
                else
                {
                    MessageBox.Show("Кол-во узлов N - это натуральное число от 1 до 100 включительно.", "Ошибка");
                }
            }
            catch
            {
                MessageBox.Show("Введите в поля натуральные числа не равные 0.", "Ошибка");
            }
        }

        private void button6_Click(object sender, EventArgs e) //Заполнение матрицы инцидентности на основе матрици смежности и вызов button7
        {
            bool Successful = true;
            try
            {
                for(int i=0; i<dataGridView3.RowCount; i++) //Проверка значений
                {
                    for(int j=0; j<dataGridView3.ColumnCount; j++)
                    {
                        if(Convert.ToInt32(dataGridView3[j, i].Value) != 1 && Convert.ToInt32(dataGridView3[j, i].Value) != 0)
                        {
                            Successful = false;
                        }
                    }
                }
                if(Successful)
                {
                    int e_count = 0;
                    for(int i=0; i<dataGridView3.RowCount; i++) //считаем число узлов
                    {
                        for(int j=0; j<dataGridView3.ColumnCount; j++)
                        {
                            if(Convert.ToInt32(dataGridView3[j, i].Value)==1)
                            {
                                e_count++;
                            }
                        }
                    }
                    if (e_count > 0)
                    {
                        textBox3.Text = Convert.ToString(dataGridView3.RowCount);
                        textBox4.Text = Convert.ToString(e_count);
                        button5.PerformClick(); //формируем матрицу инцидентности

                        for (int i = 0; i < dataGridView2.ColumnCount; i++) //предварительно очищаем матрицу инцидентности
                        {
                            for (int j = 0; j < dataGridView2.RowCount; j++)
                            {
                                dataGridView2[i, j].Value = null;
                            }
                        }

                        for (int i = 0; i < dataGridView3.RowCount; i++) //заполняем матрицу инцидентности
                        {
                            for (int j = 0; j < dataGridView3.ColumnCount; j++)
                            {
                                if (Convert.ToInt32(dataGridView3[j, i].Value) == 1 && j != i)
                                {
                                    dataGridView2[e_count - 1, i].Value = 1;
                                    dataGridView2[e_count - 1, j].Value = -1;
                                    e_count--;
                                }
                                if (Convert.ToInt32(dataGridView3[j, i].Value) == 1 && j == i)
                                {
                                    dataGridView2[e_count - 1, i].Value = 1;
                                    e_count--;
                                }
                            }
                        }

                        button7.PerformClick(); //формируем граф
                    }
                    else
                    {
                        MessageBox.Show("Граф должен иметь хотя бы одну связь. Введите 1 в какую-нибудь ячейку\nматрици смежности и повторите попытку.", "Ошибка ввода");
                    }
                }
                else
                {
                    MessageBox.Show("В каждой ячейке матрицы смежности должен быть 0 или 1.\nПустые ячейки автоматически заполняются нулями.", "Ошибка ввода");
                }
            }
            catch
            {
                MessageBox.Show("В каждой ячейке матрицы смежности должен быть 0 или 1.\nПустые ячейки автоматически заполняются нулями.","Ошибка ввода");
            }
        }

        private void button4_Click(object sender, EventArgs e) //Удаление узла (Матр. смежности)
        {
            try
            {
                int numb_uz = Convert.ToInt32(textBox2.Text);
                if (numb_uz >= 0 && numb_uz < dataGridView3.RowCount)
                {
                    if (dataGridView3.RowCount != 1)
                    {
                        dataGridView3.Rows.RemoveAt(numb_uz);
                        dataGridView3.Columns.RemoveAt(numb_uz);
                        for (int i = 0; i < dataGridView3.ColumnCount; i++)
                        {
                            dataGridView3.Columns[i].HeaderText = "v[" + i + "]";
                            dataGridView3.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dataGridView3.Rows[i].HeaderCell.Value = "v[" + i + "]";
                        }
                        textBox1.Text = Convert.ToString(dataGridView3.RowCount);
                    }
                    else
                    {
                        MessageBox.Show("Нельзя удалить последний узел в графе.", "Ошибка ввода");
                    }
                }
                else
                {
                    MessageBox.Show("Нет узла с таким номером.", "Ошибка ввода");
                }
            }
            catch
            {
                MessageBox.Show("Для удаления узла введите его номер (натуральное число) в поле.", "Ошибка ввода");
            }
            textBox2.Text = "";
        }

        private void IsolationLevel() //Иерархические уровни
        {
            ListRect2 = new List<Rectangle>();
            ListLevel = new List<List<int>>();
            int counter = 0, counterEL = 0;
            List<int> tmpL = new List<int>();
            for (int i = 0; i < dataGridView3.ColumnCount; i++)
            {
                for (int j = 0; j < dataGridView3.RowCount; j++)
                {
                    if (Convert.ToInt32(dataGridView3[i, j].Value) == 1)
                    {
                        counter++;
                    }
                }
                if (counter == 0)
                {
                    tmpL.Add(i);
                    counterEL++;
                }
                counter = 0;
            }
            counter = 0;
            while (tmpL.Count != 0)
            {
                bool Access = false, Access1 = false;
                ListLevel.Add(tmpL);
                tmpL = new List<int>();
                for (int i = 0; i < dataGridView3.ColumnCount; i++)
                {
                    Access = true;
                    for (int l = 0; l < ListLevel.Count; l++)
                    {
                        for (int el = 0; el < ListLevel[l].Count; el++)
                        {
                            if (i == ListLevel[l][el]) { Access = false; break; }
                        }
                        if (Access == false) break;
                    }
                    if (Access)
                    {
                        for (int j = 0; j < dataGridView3.RowCount; j++)
                        {
                            if (Convert.ToInt32(dataGridView3[i, j].Value) == 1)
                            {
                                Access1 = false;
                                for (int l = 0; l < ListLevel.Count; l++)
                                {
                                    for (int el = 0; el < ListLevel[l].Count; el++)
                                    {
                                        if (j == ListLevel[l][el]) { Access1 = true; break; }
                                    }
                                    if (Access1 == true) break;
                                }
                            }
                            else
                            {
                                continue;
                            }
                            if (Access1 == false) break;
                        }
                        if (Access1 == true)
                        {
                            tmpL.Add(i);
                            counterEL++;
                        }
                    }
                }
            }
            if (counterEL == dataGridView3.ColumnCount)
            {
                DrawAccess = true;
                label2.Visible = false;
                pictureBox2.Visible = true;
                counter = 0;
                for (int i = 0; i < ListLevel.Count; i++)
                {
                    for (int j = 0; j < ListLevel[i].Count; j++)
                    {
                        ListRect2.Add(new Rectangle(20 + i * 100, 50 + j * 100, 50, 50));
                    }
                }
            }
            else
            {
                label2.Visible = true;
                pictureBox2.Visible = false;
                DrawAccess = false;
            }
        }

        private void StructTopologCharact() //Расчеты
        {
            //Сведение графа к неорейтированному без петель
            DataGridView NeOreyntirGraph = new DataGridView();
            NeOreyntirGraph.RowCount = dataGridView3.RowCount;
            NeOreyntirGraph.ColumnCount = dataGridView3.ColumnCount;
            for(int i=0; i<dataGridView3.RowCount; i++)
            {
                for (int j = 0; j < dataGridView3.ColumnCount; j++)
                {
                    if (NeOreyntirGraph[j, i].Value == null)
                    {
                        NeOreyntirGraph[j, i].Value = 0;
                    }
                    if (i!=j && Convert.ToInt32(dataGridView3[j,i].Value)==1)
                    {
                        NeOreyntirGraph[j, i].Value = 1;
                        NeOreyntirGraph[i, j].Value = 1;
                    }
                }
            }
            
            int m = 0, n = NeOreyntirGraph.ColumnCount; //m - число ребер, n - число узлов.
            for (int i = 0; i < NeOreyntirGraph.ColumnCount; i++)
            {
                for (int j = 0; j < NeOreyntirGraph.ColumnCount; j++)
                {
                    if (Convert.ToInt32(NeOreyntirGraph[j, i].Value) == 1)
                    {
                        m++;
                    }
                }
            }
            m = m / 2;

            //Структурная избыточность
            label3.Visible = true;
            if (m >= 1)
            {
                label3.ForeColor = Color.Black;
                label3.Text = "Все кратчайшие пути графа без дуг и петель, в котором длина любого ребра равна 1:";
                dataGridView4.Visible = true;
                label4.Visible = true;
                dataGridView5.Visible = true;
                double R = ((double)m / (n - 1)) - 1;

                //Цикл подсчета всех кратчайших путей
                int counter = 0;
                int[,] result = new int[NeOreyntirGraph.ColumnCount, NeOreyntirGraph.ColumnCount];
                int[] need_to_go;
                for (int k = 0; k < NeOreyntirGraph.ColumnCount; k++)
                {
                    int[] dliny = new int[NeOreyntirGraph.ColumnCount];
                    for (int i = 0; i < NeOreyntirGraph.ColumnCount; i++) dliny[i] = -1; //Заполняем массив путей -1 (считаем, что -1 - это бесконечно большое число)
                    dliny[k] = 0; //в первый элемнт пути мы можем добраться за 0 шагов.
                    int dlina = 1;
                    counter = 0;
                    for (int j = 0; j < NeOreyntirGraph.ColumnCount; j++) //смотрим из каких узлов можно добраться до первого и в какие можно попасть из первого. Записываем для этих узлов длину пути 1
                    {
                        if ((Convert.ToInt32(NeOreyntirGraph[j, k].Value) == 1 || Convert.ToInt32(NeOreyntirGraph[k, j].Value) == 1) && j != k)
                        {
                            if (dliny[j] == -1 || dliny[j] > dlina)
                            {
                                dliny[j] = dlina;
                                counter++;
                            }
                        }
                    }
                    if (counter == 0) //Если узлы были найдены, то ищем остальные кратчайшие пути в цикле
                    {
                        continue;
                    }
                    while (counter != 0)
                    {
                        need_to_go = new int[counter];
                        counter = 0;
                        for (int s = 0; s < NeOreyntirGraph.ColumnCount; s++) //Вершины, которые нужно обойти
                        {
                            if (dliny[s] == dlina)
                            {
                                need_to_go[counter] = s;
                                counter++;
                            }
                        }
                        counter = 0;
                        dlina++;
                        for (int i = 0; i < need_to_go.Length; i++)
                        {
                            for (int j = 0; j < NeOreyntirGraph.ColumnCount; j++)
                            {
                                if ((Convert.ToInt32(NeOreyntirGraph[j, need_to_go[i]].Value) == 1 || Convert.ToInt32(NeOreyntirGraph[need_to_go[i], j].Value) == 1) && j != need_to_go[i])
                                {
                                    if (dliny[j] == -1 || dliny[j] > dlina)
                                    {
                                        dliny[j] = dlina;
                                        counter++;
                                    }
                                }
                            }
                        }
                    }
                    for (int i = 0; i < dliny.Length; i++)
                    {
                        result[k, i] = dliny[i];
                    }
                }

                //Определяем связность структуры
                label1.Visible = true;
                Svyaznost = true;
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (result[i, j] == -1)
                        {
                            Svyaznost = false;
                            break;
                        }
                    }
                    if (Svyaznost == false)
                    {
                        break;
                    }
                }
                if (Svyaznost)
                {
                    label1.Text = "Граф является Связным";
                    label1.ForeColor = Color.Green;
                }
                else
                {
                    label1.Text = "Граф не является Связным";
                    label1.ForeColor = Color.Red;
                }

                dataGridView4.ColumnCount = n;
                dataGridView4.RowCount = n;
                dataGridView4.ColumnHeadersVisible = true;
                dataGridView4.RowHeadersVisible = true;
                dataGridView4.RowHeadersWidth = 70;
                for (int i = 0; i < n; i++)
                {
                    dataGridView4.Columns[i].HeaderText = "v[" + i + "]";
                    dataGridView4.Rows[i].HeaderCell.Value = "v[" + i + "]";
                }
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        dataGridView4[j, i].Value = result[i, j];
                    }
                }

                //Параметр e^2, учитывающий неравномерность распределения связей
                if (R >= 0)
                {
                    double g_sr = 0, e2 = 0;
                    double[] g = new double[n];
                    for (int i = 0; i < n; i++)
                    {
                        g[i] = 0;
                    }
                    g_sr = (double)(2 * m) / n;
                    for (int i = 0; i < NeOreyntirGraph.ColumnCount; i++)
                    {
                        for (int j = 0; j < NeOreyntirGraph.ColumnCount; j++)
                        {
                            if (i != j && (Convert.ToInt32(NeOreyntirGraph[j, i].Value) == 1 || Convert.ToInt32(NeOreyntirGraph[i, j].Value) == 1))
                            {
                                g[i]++;
                            }
                        }
                    }
                    for (int i = 0; i < n; i++)
                    {
                        e2 += (g[i] - g_sr) * (g[i] - g_sr);
                    }

                    int Q = 0; //абсолютная компактность
                    for (int i = 0; i < NeOreyntirGraph.ColumnCount; i++)
                    {
                        for (int j = 0; j < NeOreyntirGraph.ColumnCount; j++)
                        {
                            Q += result[i, j];
                        }
                    }
                    int Qmin = NeOreyntirGraph.ColumnCount * (NeOreyntirGraph.ColumnCount - 1); //Минимальное значение компактности для структуры типа "полный граф"
                    double Qotn = 0;
                    if (Qmin != 0)
                    {
                        Qotn = ((double)Q / Qmin) - 1; //Относительная компактность
                    }
                    int max_dlina = 0; //Максимальная длина пути среди кратчайших путей из i в j
                    for (int i = 0; i < NeOreyntirGraph.ColumnCount; i++)
                    {
                        for (int j = 0; j < NeOreyntirGraph.ColumnCount; j++)
                        {
                            if (result[i, j] > max_dlina)
                            {
                                max_dlina = result[i, j];
                            }
                        }
                    }

                    //Степень централизации
                    double[] Z = new double[n];
                    int[] sum_dlin = new int[n];
                    double b = 0;
                    for (int i = 0; i < n; i++)
                    {
                        sum_dlin[i] = 0;
                        for (int j = 0; j < n; j++)
                        {
                            sum_dlin[i] += result[i, j];
                        }
                    }
                    if (n != 1)
                    {
                        for (int i = 0; i < n; i++)
                        {
                            Z[i] = (double)Q / (2 * sum_dlin[i]);
                        }
                        double Z_max = 0;
                        for (int i = 0; i < n; i++)
                        {
                            if (Z[i] > Z_max)
                            {
                                Z_max = Z[i];
                            }
                        }
                        b = ((n - 1) * (2 * Z_max - n)) / (Z_max * (n - 2));
                    }
                    dataGridView5.RowCount = 1;
                    dataGridView5.RowHeadersVisible = false;
                    dataGridView5[0, 0].Value = Math.Round(R, 5);
                    dataGridView5[1, 0].Value = Math.Round(e2, 5);
                    dataGridView5[2, 0].Value = Math.Round(Qotn, 5);
                    dataGridView5[3, 0].Value = max_dlina;
                    if (double.IsNaN(b))
                    {
                        dataGridView5[4, 0].Value = "-";
                    }
                    else
                    {
                        dataGridView5[4, 0].Value = Math.Round(b, 5);
                    }
                }
                else
                {
                    dataGridView5.RowCount = 1;
                    dataGridView5.RowHeadersVisible = false;
                    dataGridView5[0, 0].Value = Math.Round(R, 5);
                    dataGridView5[1, 0].Value = "-";
                    dataGridView5[2, 0].Value = "-";
                    dataGridView5[3, 0].Value = "-";
                    dataGridView5[4, 0].Value = "-";
                }
            }
            else
            {
                label1.Hide();
                label3.ForeColor = Color.Red;
                label3.Text = "Для расчета характеристик необходимы хотя бы 2 вершины и связь между ними.";
                dataGridView4.Visible = false;
                label4.Visible = false;
                dataGridView5.Visible = false;
            }
        }

        private void Shortest_Paths() //Нахождение кратчайших путей
        {
            richTextBox1.Text = "";
            if(Svyaznost)
            {
                bool Petlya = false;
                for (int i = 0; i < Graph.RowCount; i++)
                {
                    if(Convert.ToInt32(Graph[i,i].Value)==1)
                    {
                        Petlya = true;
                        break;
                    }
                }
                if (Petlya == false)
                {
                    label11.Visible = false;
                    int counterIn = 0, counterOut = 0;
                    bool OnlyIn, OnlyOut;
                    int Consumer = -1, Provider = -1;
                    for (int i = 0; i < Graph.RowCount; i++)
                    {
                        OnlyIn = true; OnlyOut = true;
                        for (int j = 0; j < Graph.ColumnCount; j++)
                        {
                            if (Convert.ToInt32(Graph[j, i].Value) == 1)
                            {
                                OnlyIn = false;
                            }
                            if (Convert.ToInt32(Graph[i, j].Value) == 1)
                            {
                                OnlyOut = false;
                            }
                        }
                        if (OnlyIn)
                        {
                            counterIn++;
                            Consumer = i;
                        }
                        if (OnlyOut)
                        {
                            counterOut++;
                            Provider = i;
                        }
                    }
                    if (counterIn == 1 && counterOut == 1)
                    {
                        label11.Visible = false;
                        bool Complete = false;
                        List<List<int>> AllPaths = new List<List<int>>();
                        
                        for(int i=0; i<Graph.ColumnCount; i++)
                        {
                            if(Convert.ToInt32(Graph[i,Provider].Value)==1)
                            {
                                AllPaths.Add(new List<int>());
                                AllPaths[AllPaths.Count - 1].Add(Provider);
                                AllPaths[AllPaths.Count - 1].Add(i);
                            }
                        }

                        while (Complete == false)
                        {
                            for (int i = 0; i < AllPaths.Count; i++)
                            {
                                int counter = 0;
                                bool Next = false;
                                for (int j = 0; j < Graph.ColumnCount; j++)
                                {
                                    int poslednElArr;
                                    if(counter==0)
                                    {
                                        poslednElArr = AllPaths[i][AllPaths[i].Count - 1];
                                    }
                                    else
                                    {
                                        poslednElArr = AllPaths[i][AllPaths[i].Count - 2];
                                    }
                                    if (AllPaths[i][AllPaths[i].Count - 1] != -1)
                                    {
                                        if (Convert.ToInt32(Graph[j, poslednElArr].Value) == 1)
                                        {
                                            bool NeProxodili = true;
                                            for (int s = 0; s < AllPaths[i].Count; s++)
                                            {
                                                if (AllPaths[i][s] == j)
                                                {
                                                    NeProxodili = false;
                                                    break;
                                                }
                                            }
                                            if (NeProxodili)
                                            {
                                                if (counter == 0)
                                                {
                                                    AllPaths[i].Add(j);
                                                    counter++;
                                                }
                                                else
                                                {
                                                    AllPaths.Add(new List<int>());
                                                    for (int s = 0; s < AllPaths[i].Count - 1; s++)
                                                    {
                                                        AllPaths[AllPaths.Count - 1].Add(AllPaths[i][s]);
                                                    }
                                                    AllPaths[AllPaths.Count - 1].Add(j);
                                                    counter++;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Next = true;
                                        break;
                                    }
                                }
                                if(Next)
                                {
                                    continue;
                                }
                                if(counter==0)
                                {
                                    AllPaths[i].Add(-1);
                                }
                            }
                            Complete = true;
                            for (int i=0; i<AllPaths.Count; i++)
                            {
                                if (AllPaths[i][AllPaths[i].Count-1]!=-1)
                                {
                                    Complete = false;
                                    break;
                                }
                            }
                        }
                        
                        richTextBox1.Text = "Все возможные пути от поставщика "+Provider+ " до потребителя " + Consumer +":\n\n";
                        for(int i=0; i<AllPaths.Count; i++)
                        {
                            AllPaths[i].RemoveAt(AllPaths[i].Count-1);
                            for(int j=0; j<AllPaths[i].Count; j++)
                            {
                                richTextBox1.Text += AllPaths[i][j] + " ";
                            }
                            richTextBox1.Text += "\n";
                        }
                        richTextBox1.Text += "(Если путь не привоит к потребителю,значит он содержит цикл и не является кратчайшим)\n";
                        int MinLangth = 1000, MinPath = -1;
                        for (int i=0; i<AllPaths.Count; i++)
                        {
                            if(AllPaths[i].Count<MinLangth && AllPaths[i][AllPaths[i].Count-1]==Consumer)
                            {
                                MinPath = i;
                                MinLangth = AllPaths[MinPath].Count;
                            }
                        }
                        MinLangth = MinLangth - 1;
                        richTextBox1.Text += "\nКратчайшим является путь длиной "+ MinLangth +":\n";
                        for(int i=0; i<AllPaths[MinPath].Count; i++)
                        {
                            richTextBox1.Text += AllPaths[MinPath][i] + " ";
                        }
                        KratchPath = AllPaths[MinPath];
                        Access_PB4 = true;
                    }
                    else
                    {
                        label11.Visible = true;
                        label11.ForeColor = Color.Red;
                        label11.Text = "Должены быть только 1 поставщик и 1 потребитель!";
                    }
                }
                else
                {
                    label11.Visible = true;
                    label11.ForeColor = Color.Red;
                    label11.Text = "Граф не должен содержать петли!";
                }
            }
            else
            {
                label11.Visible = true;
                label11.ForeColor = Color.Red;
                label11.Text = "Задан несвязный граф!";
            }
        }

        private void button1_Click(object sender, EventArgs e)  //Сброс
        {
            panel3.Hide();
            panel4.Hide();
            button2.Hide();

            dataGridView4.Visible = false;
            label4.Visible = false;
            dataGridView5.Visible = false;
            label1.Visible = false;
            label3.Visible = false;

            label2.Visible = false;

            label11.Visible = false;

            richTextBox1.Text = "";

            Graph = null;
            GraphListRect = null;
            GraphListColor = null;

            if (dataGridView2.RowCount != 0)
            {
                for (int j = 0; j < dataGridView2.RowCount; j++)
                {
                    dataGridView2.Rows.RemoveAt(0);
                    j--;
                }
            }
            if (dataGridView2.ColumnCount != 0)
            {
                for (int i = 0; i < dataGridView2.ColumnCount; i++)
                {
                    dataGridView2.Columns.RemoveAt(0);
                    i--;
                }
            }

            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";

            if (dataGridView3.RowCount != 0)
            {
                for (int j = 0; j < dataGridView3.RowCount; j++)
                {
                    dataGridView3.Rows.RemoveAt(0);
                    j--;
                }
            }
            if (dataGridView3.ColumnCount != 0)
            {
                for (int i = 0; i < dataGridView3.ColumnCount; i++)
                {
                    dataGridView3.Columns.RemoveAt(0);
                    i--;
                }
            }

            textBox1.Text = "";
            textBox2.Text = "";

            if (dataGridView6.RowCount != 0)
            {
                for (int j = 0; j < dataGridView6.RowCount; j++)
                {
                    dataGridView6.Rows.RemoveAt(0);
                    j--;
                }
            }
            if (dataGridView6.ColumnCount != 0)
            {
                for (int i = 0; i < dataGridView6.ColumnCount; i++)
                {
                    dataGridView6.Columns.RemoveAt(0);
                    i--;
                }
            }



            Graphics a = pictureBox1.CreateGraphics();
            a.Clear(Color.White);
            a = pictureBox2.CreateGraphics();
            a.Clear(Color.White);
            a = pictureBox4.CreateGraphics();
            a.Clear(Color.White);
            Access_PB = false;
            Access_PB4 = false;
            DrawAccess = false;
        }

        private void button10_Click(object sender, EventArgs e) //Открытие
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "grph files (*.grf)|*.grf";
            OFD.RestoreDirectory = true;
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                StreamReader SR = new StreamReader(OFD.FileName);
                string str;
                string[] split;
                List<int[]> OpenGraph = new List<int[]>();
                str = SR.ReadLine();
                int V_count = Convert.ToInt32(str);
                for (int i = 0; i < V_count; i++)
                {
                    str = SR.ReadLine();
                    split = str.Split(' ');
                    int[] stroka = new int[V_count];
                    for (int j = 0; j < V_count; j++)
                    {
                        stroka[j] = Convert.ToInt32(split[j]);
                    }
                    OpenGraph.Add(stroka);
                }

                textBox1.Text = Convert.ToString(V_count);

                int NowSelectedIndex = tabControl1.SelectedIndex;
                tabControl1.SelectedIndex = 0;
                button3.PerformClick();

                for (int i = 0; i < dataGridView3.RowCount; i++)
                {
                    for (int j = 0; j < dataGridView3.ColumnCount; j++)
                    {
                        dataGridView3[j, i].Value = OpenGraph[i][j];
                    }
                }

                ListPoints = new List<Point>();
                for (int i = 0; i < V_count; i++)
                {
                    str = SR.ReadLine();
                    split = str.Split(' ');
                    ListPoints.Add(new Point(Convert.ToInt32(split[0]), Convert.ToInt32(split[1])));
                }
                SR.Close();
                button6.PerformClick();

                tabControl1.SelectedIndex = NowSelectedIndex;
            }
        }

        private void button2_Click(object sender, EventArgs e) //Сохранение
        {
            if (Graph != null)
            {
                SaveFileDialog SFD = new SaveFileDialog();
                SFD.Filter = "grph files (*.grf)|*.grf";
                SFD.RestoreDirectory = true;
                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter SW = new StreamWriter(SFD.FileName);
                    SW.WriteLine(Graph.RowCount);
                    for (int i = 0; i < Graph.RowCount; i++)
                    {
                        for (int j = 0; j < Graph.ColumnCount; j++)
                        {
                            if (j != (Graph.ColumnCount - 1))
                            {
                                SW.Write(Convert.ToString(Graph[j, i].Value) + " ");
                            }
                            else
                            {
                                SW.Write(Convert.ToString(Graph[j, i].Value) + "\r\n");
                            }
                        }
                    }
                    for (int i = 0; i < GraphListRect.Count; i++)
                    {
                        SW.WriteLine(GraphListRect[i].X + " " + GraphListRect[i].Y);
                    }
                    SW.Close();
                }
            }
            else
            {
                MessageBox.Show("Чтобы сохранить граф, нужно его создать или открыть.\nПосле того, как граф отобразится на первой вкладке, вы сможете его сохранить.","Нет графа");
            }
        }
    }
}